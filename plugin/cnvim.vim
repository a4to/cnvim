
" CNvim - a set of useful functions for neovim

" --------------------
" AutoCompleteSnippets:
" --------------------

" If using 'snip' (https://gitlab.com/a4to/snip) for snippets, automatically
" adds the 'A' trigger to the end of the snippet if it's not already there,
" for all AutoFill snippets.

function! AutoCompleteSnippets()
    let l:line_number = 1
    while l:line_number <= line('$')
        let l:line = getline(l:line_number)
        if l:line =~ '^snippet'
            let l:autocompletion_line = l:line

            if l:autocompletion_line =~ ' "$'
                let l:autocompletion_line = l:autocompletion_line . ' '
                call setline(l:line_number, l:autocompletion_line)
            endif

            if l:autocompletion_line !~ 'A$' && l:autocompletion_line !~# '" .*\C\ca.*'
                if l:autocompletion_line !~ '"$'
                    let l:autocompletion_line = l:autocompletion_line . ' "" A'
                else
                    let l:autocompletion_line = l:autocompletion_line . 'A'
                endif
                call setline(l:line_number, l:autocompletion_line)
            endif
            
            if l:autocompletion_line =~ '"A'
                let l:autocompletion_line = substitute(l:autocompletion_line, '"A', '" A', 'g')
                call setline(l:line_number, l:autocompletion_line)
            endif
        endif
        let l:line_number += 1
    endwhile
endfunction

augroup AutoCompleteSnippetsOnSave
    autocmd!
    autocmd BufWritePre AutoFill-p1.snippets call AutoCompleteSnippets()
augroup END


" --------------------
" CapitalizeLongWords:
" --------------------

" Capitalizes all words in the selected area that are longer than 3 characters.
" This is useful for converting camelCase to CamelCase, for example, or for
" capitalizing all words in a heading or title.

function! CapitalizeLongWords()
  let save_cursor = getpos('.')
  let start_line = line("'<")
  let end_line = line("'>")

  for line_num in range(start_line, end_line)
    let line = getline(line_num)
    let new_line = ''
    let in_word = 0
    let word = ''
    
    for char in split(line, '\zs')
      if char =~# '\w'
        let in_word = 1
        let word .= char
      else
        if in_word && len(word) > 3
          let word = substitute(word, '\v(\w)(\w+)', '\u\1\L\2', '')
        endif
        let new_line .= word . char
        let in_word = 0
        let word = ''
      endif
    endfor
    
    if in_word && len(word) > 3
      let word = substitute(word, '\v(\w)(\w+)', '\u\1\L\2', '')
    endif
    let new_line .= word
    
    call setline(line_num, new_line)
  endfor

  call setpos('.', save_cursor)
endfunction


" --------------------
"    AddLineBreaks:
" --------------------

" Adds line breaks to the selected area after 80 characters, but only if there
" is a space before the 80th character. This is useful for adding line breaks
" to long paragraphs of text, but not for code.


function! AddLineBreaks()
    let l:original_pos = getpos('.')
    normal! gv"ay
    let l:original_reg = getreg('a')

    " Replace the selected area with line breaks after 80 characters
    let l:lines = split(l:original_reg, "\n")
    let l:new_text = []
    for l:line in l:lines
        let l:new_line = ''
        let l:line_len = len(l:line)
        let l:pos = 0
        while l:pos < l:line_len
            let l:end_pos = min([l:pos + 80, l:line_len])
            let l:segment = strpart(l:line, l:pos, l:end_pos - l:pos)

            " Search backwards from the end of the segment for a space
            let l:space_pos = matchend(l:segment, '.* ')
            if l:space_pos != -1 && l:end_pos != l:line_len
                let l:end_pos = l:pos + l:space_pos
                let l:segment = strpart(l:line, l:pos, l:end_pos - l:pos)
            endif

            let l:new_line .= l:segment
            let l:pos = l:end_pos
            if l:pos < l:line_len
                let l:new_line .= "\n"
            endif
        endwhile
        call add(l:new_text, l:new_line)
    endfor

    " Update the selected area with the modified text
    call setreg('a', join(l:new_text, "\n"))
    normal! gv"ap

    " Restore the original cursor position
    call setpos('.', l:original_pos)
endfunction


" --------------------
"     ColumnTotal:
" --------------------

" Adds a line to the end of the selected area with the total of each column
" that contains numbers. This is useful for adding up columns of numbers in
" spreadsheets or tables.


function! ColumnTotal() range
    let column_totals = 0

    for lnum in range(a:firstline, a:lastline)
        let line = getline(lnum)
        let items = split(line, '\t')

        for item in items
            let number = matchstr(item, '\d\+\.\?\d*')
            if number != ''
                let column_totals += str2float(number)
            endif
        endfor
    endfor

    if column_totals > 0
        call append(line('$'), 'Total: ' . column_totals)
    endif
endfunction

" --------------------
"  Arrow Functionizer:
" --------------------

" Converts ES5 function declarations to ES6 arrow functions.

function! ConvertToArrowFunctions()
    let l:line_number = 1
    while l:line_number <= line('$')
        let l:line = getline(l:line_number)

        if l:line =~ '(async function.*{$'
            let l:line = substitute(l:line, '\v\(async function (.{-})\((.*)\) \{', "(async (\\2) => {", "")
            call setline(l:line_number, l:line)
        endif

        if l:line =~ 'async function.*{$'
            let l:line = substitute(l:line, '\vasync function (.{-})\((.*)\) \{', "const \\1 = async (\\2) => {", "")
            call setline(l:line_number, l:line)
        endif

        if l:line =~ 'function.*{$'
            let l:line = substitute(l:line, '\vfunction (.{-})\((.*)\) \{', "const \\1 = (\\2) => {", "")
            call setline(l:line_number, l:line)
        endif

        let l:line_number += 1
    endwhile
endfunction


let g:cnvim_reload_on_save = 0

function! cnvim#ReloadBrowser()
    if g:cnvim_reload_on_save
        silent exec '!'.expand('$BROWSER') . ' --reload ' . expand('%:p')
    endif
endfunction

function! cnvim#ToggleLiveReload()
    let g:cnvim_reload_on_save = !g:cnvim_reload_on_save
endfunction

augroup cnvim
    autocmd!
    autocmd BufWritePost * call cnvim#ReloadBrowser()
augroup END

