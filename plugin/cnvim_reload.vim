
let g:cnvim_reload_reload_on_save = 0

function! cnvim_reload#ReloadBrowser()
    if g:cnvim_reload_reload_on_save
        silent exec '!'.expand('$BROWSER') . ' --reload ' . expand('%:p')
    endif
endfunction

function! cnvim_reload#ToggleLiveReload()
    let g:cnvim_reload_reload_on_save = !g:cnvim_reload_reload_on_save
endfunction

function! cnvim_reload#StopReloadServer()
    let l:cmd = 'kill ' . g:cnvim_reload_pid
    call system(l:cmd)
endfunction

function! cnvim_reload#ToggleReloadServer()
    if exists('g:cnvim_reload_pid')
      call cnvim_reload#StopReloadServer()
    else
      call cnvim_reload#ReloadServer()
    endif
endfunction

function! cnvim_reload#ReloadServer()
    let l:current_file = expand('%:p')
    let l:scriptPath = expand('$HOME/.vim/plugged/cnvim/plugin/cnvim-reload.js')
    if filereadable(l:scriptPath)
        let l:scriptDir = expand('$HOME/.vim/plugged/cnvim/plugin')
    else
        let l:scriptDir = expand('$HOME/.config/nvim/plugged/cnvim/plugin')
    endif
    let l:server = l:scriptDir . '/cnvim-reload.js'
    let l:cmd = 'node ' . l:server . ' ' . l:current_file . ' &'
    let l:pid = system(l:cmd)
    let g:cnvim_reload_pid = l:pid
endfunction

"augroup cnvim_reload
"    autocmd!
"    autocmd BufWritePost * call cnvim_reload#ReloadBrowser()
"    autocmd VimLeave * call cnvim_reload#StopReloadServer()
"augroup END


