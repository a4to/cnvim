
const fs = require('fs');
const http = require('http');
const WebSocket = require('ws');

let filePath = process.argv[2];

const wss = new WebSocket.Server({ port: 8080 });

fs.watch(filePath, (eventType, filename) => {
    if (filename && eventType === 'change') {
        console.log(filename, 'changed');
        wss.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                client.send('reload');
            }
        });
    }
});

http.createServer().listen(7800);

